package az.ingress.ms6;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HelloCamunda implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Hello camunda for MS6, process instance id is {}",
                execution.getProcessInstanceId());
        execution.getProcessInstance().setVariable("age", 20);
        log.info("Completed");
    }
}
