package az.ingress.bpm.orchestrator.trip;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.Random;

@Slf4j
public class RserveCar implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Reserving car {}", execution.getProcessInstanceId());
       //execution.getProcessInstanceId() : as idempotency key
        Random random = new Random();
        if (random.nextBoolean()) {
            log.info("Successfully reserved car {}", execution.getProcessInstanceId());
        } else {
            log.info("Failed to reserve car {}", execution.getProcessInstanceId());
            throw new RuntimeException("Failed to reserve car");
        }
    }
}
